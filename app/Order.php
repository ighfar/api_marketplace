<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
      protected $table = 'orders';
   protected $guarded = ['id']; 

 function user(){
      return $this->hasMany('App\User','order_id','id');
    
}

 function cart(){
      return $this->hasMany('App\Cart','order_id','id');
    
}

   protected $casts = [
   		'ekspedisi' => 'array',
   	];
}
