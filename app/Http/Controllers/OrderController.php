<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Response;

class OrderController extends Controller
{
     public function index() {
        return Order::all();
    }
    public function store(Request $request)
    {

	$order=new Order;
	$order->user_id=$request->user_id;
	$order->cart_id=$request->cart_id;
	$order->ekspedisi=$request->ekspedisi;
	$order->total=$request->total;
	$order->status=$request->status;
	$success=$order->save();
 
	if(!$success)
	{
       	     return Response()->json("error saving",500);
	}else{
 
        return Response()->json("success",201);

}
}     
}
