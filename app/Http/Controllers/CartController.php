<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Response;

class CartController extends Controller
{
    
	 public function index() {
        return Cart::all();
    }
    public function store(Request $request)
    {

	$cart=new Cart;
	$cart->product_id=$request->product_id;
	$cart->qty=$request->qty;
	$cart->ukuran=$request->ukuran;
	$cart->catatan=$request->catatan;
	$cart->subtotal=$request->subtotal;
	$success=$cart->save();
 
	if(!$success)
	{
       	     return Response()->json("error saving",500);
	}else{
 
        return Response()->json("success",201);

}
}     

}
