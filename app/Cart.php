<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
   	protected $table = 'carts';
	protected $guarded = ['id']; 

 function product(){
    	return $this->hasMany('App\Product','cart_id','id');
    }
}
