<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('cart', 'CartController@store');
Route::get('list_cart', 'CartController@index');
Route::post('order', 'OrderController@store');
Route::get('list_order', 'OrderController@index');
Route::get('category', 'CategoryController@index');
Route::get('category/{category_id}', 'CategoryController@product_per_category');
Route::get('product', 'ProductController@index');
Route::get('product/{id}', 'ProductController@detail');
Route::get('profil', 'UserController@getAuthenticatedUser')->middleware('jwt.verify');
Route::put('update_user/{id}', 'UserController@update');
Route::post('change_password', 'UserController@change_password');
